package m450.unittesting.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class test {

    @Test
    @DisplayName("Adding two numbers")
    void add() {
        assertEquals(4, Main.add(2, 2));
    }

    @Test
    @DisplayName("Subtracting two numbers")
    void subtract() {
        assertEquals(0, Main.subtract(2, 2));
    }

    @Test
    @DisplayName("Multiply two numbers")
    void multiply() {
        assertEquals(4, Main.multiply(2, 2));
    }

    @Test
    @DisplayName("dividing two numbers")
    void divide() {
        assertEquals(2, Main.divide(4, 2));
    }
}
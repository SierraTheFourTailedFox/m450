package m450.unittesting.main;

public class main {
    public int add(int summand1, int summand2) {
        return summand1 + summand2;
    }
    public int subtract(int subtractor1, int subtractor2){
        return subtractor1-subtractor2;
    }
    public int multiply(int multiplier1, int multiplier2){
        return multiplier1*multiplier2;
    }
    public double divide(int divider1, int divider2){
        return (double) divider1 /divider2;
    }
}

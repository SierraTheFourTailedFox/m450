package m450.unittesting.junit5;

import ch.schule.PromoYouthSavingsAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests für das Promo-Jugend-Sparkonto.
 *
 * @author XXXX
 * @version 1.0
 */
public class PromoYouthSavingsAccountTests
{
	PromoYouthSavingsAccount promo;
	@BeforeEach
	void setUp(){
		promo = new PromoYouthSavingsAccount("Y1001");
	}

	/**
	 * Der Test.
	 */
	@Test

	public void testDeposit()
	{
		assertEquals(true, promo.deposit(2022, 1000));
	}
	@Test
	public void testGetid(){
		assertEquals("Y1001", promo.getId());
	}
	@Test
	public void testGetBalance(){
		assertEquals(0, promo.getBalance());
	}
	@Test
	public void testCanTransact(){
		assertEquals(true, promo.canTransact(2023));
	}
	@Test
	public void testWithdraw(){
		promo.deposit(2022, 1000);
		assertEquals(true, promo.withdraw(2023, 10));
	}


}

package m450.unittesting.junit5;

import ch.schule.Bank;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Tests f�r die Klasse 'Bank'.
 *
 * @author xxxx
 * @version 1.0
 */
public class BankTests {
    /**
     * Tests to create new Accounts
     */
    Bank Bank;

    @BeforeEach
    void setUp(){
        Bank = new Bank();
    }
    @Test
    public void testCreate() {
        assertEquals("S-1000", Bank.createSavingsAccount());
        assertEquals("P-1001", Bank.createSalaryAccount(0));
        assertEquals("Y-1002", Bank.createPromoYouthSavingsAccount());

    }
    /**
     * Testet das Einzahlen auf ein Konto.
     */
    @Test
    public void testDeposit() {
        Bank.createSavingsAccount();
        assertEquals(true, Bank.deposit("S-1000", 2020, 100));
    }
    /**
     * Testet das Abheben von einem Konto.
     */
    @Test
    public void testWithdraw() {
        Bank.createSavingsAccount();
        Bank.deposit("S-1000", 2020, 100);
        assertEquals(true, Bank.withdraw("S-1000", 2020, 100));
    }

    /**
     * Experimente mit print().
     */
    @Test
    public void testPrint() {
        fail("toDo");

    }

    /**
     * Experimente mit print(year, month).
     */
    @Test
    public void testMonthlyPrint() {
        fail("toDo");
    }

    /**
     * Testet den Gesamtkontostand der Bank.
     */
    @Test
    public void testBalance() {
        Bank.createSavingsAccount();
        assertEquals(0, Bank.getBalance("S-1000"));

    }

    /**
     * Tested die Ausgabe der "top 5" konten.
     */
    @Test
    public void testTop5() {
        fail("toDo");
    }

    /**
     * Tested die Ausgabe der "top 5" konten.
     */
    @Test
    public void testBottom5() {
        fail("toDo");
    }

}

package m450.unittesting.junit5;

import ch.schule.Booking;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


/**
 * Tests für die Klasse Booking.
 *
 * @author Luigi Cavuoti
 * @version 1.1
 */
public class BookingTests
{
	/**
	 * Tests f�r die Erzeugung von Buchungen.
	 */
	@Test
	public void testInitialization()
	{
		Booking b = new Booking(2020, 100);
		assertEquals(2020, b.getDate());
		assertEquals(100, b.getAmount());

	}

	/**
	 * Experimente mit print().
	 */
	@Test
	public void testPrint()
	{
		fail("toDo");
	}
}

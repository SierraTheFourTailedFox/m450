package ch.tbz.siad.junit5;

import ch.tbz.siad.Casino;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CasinoTest {
    private Casino casino;
    @BeforeEach
    private void Setup(){
        casino = new Casino();
    }
    @Test
    void TestCoinflip(){
        boolean coin_flip = casino.coinflip();
        boolean isBoolean;
        if (coin_flip == true || coin_flip == false){
            isBoolean = true;
        } else{isBoolean = false;}
        assertEquals(isBoolean, true);
    }
    @Test
    void TestDice(){
        int dice_rolled = casino.rollDice();
        assertTrue(dice_rolled > 0 && dice_rolled < 7);
    }

    @Test
    void TestPinata(){
        assertEquals(1, casino.hitPinata());
    }
}


import ch.tbz.siad.Item;
import ch.tbz.siad.ItemType;
import ch.tbz.siad.Player;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerTest {
    private Player player;
    private int playerHealth = 100;
    private ArrayList<Item> items;
    @BeforeEach
    void Setup(){
        player = new Player();
        player.setHealth(playerHealth);
        int allowedhealth = 100;
        player.setAllowedHealth(allowedhealth);
        player.addMoney(15);
        player.addItem(new Item(10,1, false, 20, ItemType.WEAPON, "Dagger"));
        player.setArmor(new Item(5,1, false, 5, ItemType.ARMOR, "Tunic"));
    }
    @Test
    void testPlayerHealth(){
        assertEquals(100, player.gethealth());
    }
    @Test
    void testPlayerDamage(){
        assertEquals(0, player.dealDamage());
    }
    @Test
    void testPlayerAllowedHealth(){
        assertEquals(100, player.getAllowedHealth());
    }
    @Test
    void testPlayerMoney(){
        assertEquals(15, player.getMoney());
    }


}

package ch.tbz.siad.junit5;

import ch.tbz.siad.Pinata;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class PinataTest {
    private Pinata pinata;
    @BeforeEach
    private void Setup(){
        pinata = new Pinata(0, 1);
    }
    @Test
    void TestBreakChance(){
        assertEquals(0, pinata.getBreakChance());
    }
    @Test
    void TestRewardValue(){
        assertEquals(1, pinata.getRewardValue());
    }
    @Test
    void TestNotBrokenPinata(){
        assertFalse(pinata.getBroken());
    }
    @Test
    void TestBrokenPinata(){
        pinata.breakPinata();
        assertTrue(pinata.getBroken());
    }
}

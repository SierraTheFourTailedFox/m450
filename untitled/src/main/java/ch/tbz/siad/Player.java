/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ch.tbz.siad;

import java.util.ArrayList;

/**
 *
 * @author sierra
 */
public class Player implements Creature{
    private int money;
    private int health;
    private int allowedhealth;
    private ArrayList<Item> items = new ArrayList<>();
    private Item armor;
    
    @Override
    public void takeDamage(int amount) {
        amount -= amount/armor.getAmount();
        health -= amount;
    }

    @Override
    public int dealDamage() {
        return 0;
    }

    @Override
    public void healDamage(int amount) {
        health += amount;
    }
    public int gethealth(){
        return health;
    }
    public void setHealth(int amount){

        health = amount;
    }
    public void setAllowedHealth(int allowed){
        allowedhealth = allowed;
    }
    public int getAllowedHealth(){
        return allowedhealth;
    }
    public void addMoney(int amount){
        money += amount;
    }
    public void subtractMoney(int amount){
        money -= amount;
    }
    public void addItem(Item item){
        items.add(item);
    }
    public int getMoney(){
        return money;
    }
    public ArrayList getWeapons(){
        return items;
    }


    public Item getArmor() {return armor;}

    public void setArmor(Item armor) {this.armor = armor;}




}

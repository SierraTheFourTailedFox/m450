package ch.tbz.siad;

import java.util.Random;

public class Casino {
    private Random random = new Random();
    private Pinata pinata = new Pinata(0, 1);
    public void showGames() {
        System.out.println("Welcome to the casino, select the game you want to play:\n(0) -> Coinflip\n(1) -> Diceroll\n(2) -> Piñata\n(3) -> Exit casino");
    }

    public boolean coinflip() {
        return random.nextBoolean();
    }

    public int rollDice() {
        return random.nextInt(5)+1;
    }
    public int hitPinata() {
        if(pinata.getBroken()){
            System.out.println("Piñata is broken, please repair.");
            return 0;
        }
        pinata.addBreakChance(1);
        if(random.nextInt(100) <= pinata.getBreakChance()){
            pinata.breakPinata();
            System.out.println("The piñata broke!");
            return 0;
        }
        System.out.println("You obtained " + pinata.getRewardValue() + " coins.");
        return pinata.getRewardValue();
    }
    public void repairBrokenPinata(){
        pinata.repairPinata();
    }
}

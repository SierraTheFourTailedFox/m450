/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ch.tbz.siad;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author sierra
 */
public class PlayGame {
    private int round = 0;
    private boolean firstTimePlaying = true; 
    private PlayerHandler Playerhandler;
    private Scanner scanner;
    private int input;
    private int money;
    public void menu() throws InputException{
        if (firstTimePlaying){
            firstTimePlaying = false;
            setup();
        }
        System.out.println("---------\nWelcome to the menu\n--------\nThese are your stats:");
        Playerhandler.getPlayerStats();
        System.out.println("--------\nWhat do you want to do?\n(1) -> Exit the castle and help the defences\n(2) -> Go to the shop\n(3) -> Enter the casino\n(4) -> Exit");
        try {
           input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }
        switch (input) {
            case 1 -> play();
            case 2 -> buyItems();
            case 3 -> casino();
            case 4 -> System.exit(0);
            default -> menu();
        }
        menu();
    }



    private void setup(){
        System.out.println("Welcome to DefenderWaves 2.0");
        Playerhandler = new PlayerHandler();
        scanner = new Scanner(System.in);
        Playerhandler.generatePlayer(round);
        Playerhandler.generateShopKeeper();
        Playerhandler.generateCasino();
        System.out.println("What difficulty do you want to play on?");
        System.out.println("(1) -> Can i play, daddy?\n(2) -> Don't hurt me\n(3) -> Bring 'em on\n(4) -> I am death incarnate\n(5) -> MEIN KAMPF\n(6) -> MEIN LEBEN\n");

        try {
            input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }


        switch (input) {
            case 1 -> Playerhandler.generateEnemy(difficultyModifier.can_i_play_daddy, round);
            case 2 -> Playerhandler.generateEnemy(difficultyModifier.dont_hurt_me, round);
            case 3 -> Playerhandler.generateEnemy(difficultyModifier.brimg_em_on, round);
            case 4 -> Playerhandler.generateEnemy(difficultyModifier.i_am_death_incarnate, round);
            case 5 -> Playerhandler.generateEnemy(difficultyModifier.MEIN_KAMPF, round);
            case 6 -> Playerhandler.generateEnemy(difficultyModifier.MEIN_LEBEN, round);
            default -> setup();
        }

        System.out.println(getRound());
    }
    private void play() throws InputException{
        System.out.println("There is an enemy, what do you do?\nThese are your stats:");
        Playerhandler.getPlayerStats();
        System.out.println("These are your enemy's stats:");
        Playerhandler.getEnemyStats();
        System.out.println("(1) -> Attack\n(2) -> Heal\n(3) -> Flee");
        try {
           input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }
        switch (input) {
            case 1 -> attack();
            case 2 -> heal();
            case 3 -> menu();
            default -> play();
        }
        checkEnemyDeath();
        getAttacked();
        checkPlayerDeath();
        play();

    }

    private void getAttacked() {
        Playerhandler.getAttacked();
    }

    private int getRound(){
        return round;
    }
    private void buyItems(){
        Playerhandler.printShopItemsAndPrices();
        System.out.println("Please enter the index of the item you want to buy.");
        try {
            input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }
        Playerhandler.buyItem(input);
    }
    private void checkPlayerDeath(){
        if(Playerhandler.checkPlayerDeath()){
            System.out.println("You died, game over!");
            System.exit(0);
        }

    }
    private void checkEnemyDeath(){
        if (Playerhandler.checkEnemyDeath(round))
        {
            System.out.println("Good work, the enemy is dead.");
        }
    }
    private void setRound(int roundToBeSet){
        round = roundToBeSet;
    }

    private int getPlayerInput() throws InputException {
        int playerinput;
        try {
            playerinput = scanner.nextInt();
        } catch (InputMismatchException e) {
            throw new InputException("Only number inputs are allowed.", e);
            
        }
        return playerinput;
    }

    private void attack() {
        Playerhandler.getWeapons();
        System.out.println("Please enter the index of the weapon you want to use.");
        try {
            input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }
        Playerhandler.attack(input);

    }

    private void heal() {
        Playerhandler.getPotions();
        System.out.println("Please enter the index of the potion you want to use.");
        try {
            input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }
        Playerhandler.heal(input);    }
    private void casino() throws InputException {
        updatePlayerMoney();
        if (money <= 0){
            System.out.println("You don't have enough money to enter the casino.");
            return;
        }
        Playerhandler.showCasinoOptions();
        System.out.println("Please enter the index of the game you want to play.");
        try {
            input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }

        switch (input) {
            case 0 -> coinflip();
            case 1 -> diceroll();
            case 2 -> pinata();
            case 3 -> menu();
            default -> casino();
        }

    }

    private void pinata() throws InputException {
        updatePlayerMoney();
        if (money < 10 && Playerhandler.returnFirstPinata()){
            System.out.println("You don't have enough money.");
            casino();
        }
        if (Playerhandler.returnFirstPinata()) {
            System.out.println("Bet 10 coins to hit the piñata for the first time?\n(1)-> Proceed\n(2)-> Abort");
        } else {
            System.out.println("Do you want to hit the piñata again?\n(1)-> Hit\n(2)-> Abort");
        }

        try {
            input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }
        switch (input){
            case 1 -> Playerhandler.hitPinata();
            case 2 -> casino();
            default -> pinata();
        }
        pinata();
    }

    private void diceroll() throws InputException {
        updatePlayerMoney();
        if (money < 5){
            System.out.println("You don't have enough money.");
            casino();
        }
        System.out.println("You will bet 5 coins on a dice roll, do you wish to proceed?\n(1) -> Proceed\n(2)-> Abort");
        try {
            input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }
        switch (input){
            case 1 -> Playerhandler.rollDice();
            case 2 -> casino();
            default -> diceroll();
        }
        casino();
    }

    private void coinflip() throws InputException {
        updatePlayerMoney();

        System.out.println(money + ": is your current money\n How much do you want to bet?");
        try {
            input = getPlayerInput();
        } catch (InputException e) {
            System.err.println(e.getMessage());
        }
        if (input > money){
            System.out.println("You can't bet more than your current balance.");
            coinflip();
        }
        Playerhandler.coinflip(input);
        casino();
    }

    private void updatePlayerMoney(){
        money = Playerhandler.getMoney();
    }

}

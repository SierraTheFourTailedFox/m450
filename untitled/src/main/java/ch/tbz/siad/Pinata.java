package ch.tbz.siad;

public class Pinata {
        private int breakChance;
        private int rewardValue;

        private boolean broken = false;

        public Pinata(int breakChance, int rewardValue) {
            this.breakChance = breakChance;
            this.rewardValue = rewardValue;
        }

        public void setBreakChance(int breakChance) {
            this.breakChance = breakChance;
        }
        public void setRewardValue(int rewardValue){
            this.rewardValue = rewardValue;
        }

        public double getBreakChance() {
            return breakChance;
        }

        public int getRewardValue() {
            return rewardValue;
        }

        public void addBreakChance(int addition){
            breakChance += addition;
        }
        public void repairPinata(){
            broken = false;
        }
        public void breakPinata(){
            broken = true;
        }

        public boolean getBroken(){
            return broken;
        }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ch.tbz.siad;


import java.util.Random;

/**
 *
 * @author sierra
 */
public class Enemy implements Creature{

    private enemyTypes type;
    private int health;
    private int damage;
    private difficultyModifier difficulty;
    Random random = new Random();
    public Enemy(difficultyModifier difficulty) {
        this.difficulty = difficulty;
    }

    public enemyTypes getType() {
        return type;
    }

    public void setType(enemyTypes type) {
        this.type = type;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public difficultyModifier getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(difficultyModifier difficulty) {
        this.difficulty = difficulty;
    }
    @Override
    public void takeDamage(int amount) {

        if (type == enemyTypes.Armored)
        {
            amount -= amount/20;
        }

        health -= amount;
    }

    @Override
    public int dealDamage() {
        return random.nextInt(damage);
    }

    @Override
    public void healDamage(int amount) {
        System.out.println("Empty function, do not use.");
    }


    public int setDamageForEnemyType() {

        switch (difficulty){
            case can_i_play_daddy -> damage += 0;
            case dont_hurt_me -> damage += 5;
            case brimg_em_on -> damage += 10;
            case i_am_death_incarnate ->damage += 15;
            case MEIN_KAMPF -> damage += 30;
            case MEIN_LEBEN -> damage += 60;
        }

        if (type == enemyTypes.Buffed ) {
            damage += 40;
        }
        else {
            damage += 20;
        }


        return damage;
    }




    
}

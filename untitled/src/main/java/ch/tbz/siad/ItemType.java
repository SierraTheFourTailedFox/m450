
package ch.tbz.siad;

/**
 *
 * @author sierra
 */
public enum ItemType {
    ARMOR, POTION, WEAPON
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ch.tbz.siad;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author sierra
 */
public class PlayerHandler {
    private Player player;
    private Enemy enemy;
    private ShopKeeper shopkeeper;
    private Casino casino;
    private int playerHealth = 100;
    private Random random = new Random();
    private ArrayList<Item> items;
    private boolean firstPinataHit = true;
    void getPlayerStats() {
        System.out.println(player.getAllowedHealth() + ": is your allowed health");
        System.out.println(player.gethealth()+ ": is your health");
        System.out.println(player.getMoney()+ ": is your money");
        //System.out.println(player.getWeapons()+ ": are your weapons");
    }
    void getEnemyStats(){
        System.out.println(enemy.getHealth() + ": Your enemy's health\n" + enemy.getType() + ": The type of your enemy");
    }
    void generatePlayer(int round) {
        player = new Player();
        player.setHealth(playerHealth);
        int allowedhealth = calculateAllowedHealth(round);
        player.setAllowedHealth(allowedhealth);
        player.addMoney(15);
        player.addItem(new Item(10,1, false, 20, ItemType.WEAPON, "Dagger"));
        player.setArmor(new Item(5,1, false, 5, ItemType.ARMOR, "Tunic"));
    }
    public void generateEnemy(difficultyModifier difficulty, int round){
        enemy = new Enemy(difficulty);
        int typenumber = random.nextInt(enemyTypes.values().length);
        enemy.setType(enemyTypes.values()[typenumber]);
        enemy.setDamage(calculateAllowedDamage(round));
        enemy.setHealth(calculateAllowedHealth(round));
        enemy.setDamageForEnemyType();

    }
    public void generateShopKeeper(){
        shopkeeper = new ShopKeeper();
        shopkeeper.generateInventory();
    }
    public void printShopItemsAndPrices(){
        items = shopkeeper.getItems();
        int index= 0;
        for (Item item : items){
            System.out.println("\n" +index + ": index\n" + item.getItemName() +":The name of the item\n"+ shopkeeper.getItemPrice(item) + ": The Price of the Item");
            index++;
        }
    }
    public void buyItem(int enteredIndex){
        items = shopkeeper.getItems();
        Item item = null;

        int index = 0;
        for (Item SHopitem : items){
            if (index == enteredIndex){
                item = SHopitem;


            }

                index++;

        }
        
        if(shopkeeper.getItemPrice(item) >= player.getMoney()){
            System.out.println("Item costs too much");
            return;
        }
        player.subtractMoney(shopkeeper.getItemPrice(item));
        if(item.getItemType() == ItemType.ARMOR){
            player.setArmor(item);
        }else {
            player.addItem(item);
        }
    }

    private int calculateAllowedDamage(int rount) {
        int damage = 20*rount + random.nextInt(10*rount + 10);
        return damage;
    }

    private int calculateAllowedHealth(int round) {
        int calculatedHealth = 100 + (round * 10);
        return calculatedHealth;
    }

    public void getWeapons() {
        items = player.getWeapons();
        int index = 0;
        for (Item item : items){
            if (item.getItemType() == ItemType.WEAPON){
                System.out.println(index + ": index\n" +item.getItemName() + ": The name of your weapon\n" + item.getAmount() + ": The Damage your Weapon deals\n" + item.calculateUsages() + ": how many times you can use this weapon");
            index++;
            }

        }
    }


    public void getArmor() {
        Item armor = player.getArmor();

        if (armor.getItemType() == ItemType.ARMOR)
        {
            System.out.println("This is your armor: " + armor.getItemName());
        }
    }

    public void attack(int weaponindex) {
        items = player.getWeapons();
        int index = 0;
        int damage = 0;
        for (Item item : items){
            if (item.getItemType() == ItemType.WEAPON) {
                if (weaponindex == index) {
                    if (item.calculateUsages()>= 0) {
                        damage = item.getAmount();
                        item.decreaseDurability();
                    }else{
                        System.out.println("This item is broken, you deal no damage, visit the Shopkeeper to repair this weapon or buy a new one");
                    }
                }
                else index++;
            }
        }
        enemy.takeDamage(damage);
    }

    public void getAttacked() {
        player.takeDamage(enemy.dealDamage());
    }

    public boolean checkPlayerDeath() {
        if (player.gethealth() <= 0){
            return true;
        }
        return false;
    }

    public boolean checkEnemyDeath(int round) {
        if (enemy.getHealth() <= 0){
            if(enemy.getHealth() == 0){
                player.setHealth(calculateAllowedHealth(round+1));
            }
            regenerateEnemy(round+1);
            player.addMoney(5);
            return true;
        }
        return false;
    }

    private void regenerateEnemy(int round) {
        int typenumber = random.nextInt(enemyTypes.values().length);
        enemy.setType(enemyTypes.values()[typenumber]);
        enemy.setDamage(calculateAllowedDamage(round));
        enemy.setHealth(calculateAllowedHealth(round));
        enemy.setDamageForEnemyType();
    }

    public void getPotions() {
        items = player.getWeapons();
        int index = 0;
        for (Item item : items){
            if (item.getItemType() == ItemType.POTION){
                System.out.println(index + ": index\n" +item.getItemName() + ": The name of your Potion\n" + item.getAmount() + ": The Health your Potions restores\n" + item.calculateUsages() + ": how many times you can use this potions");
                index++;
            }

        }
    }

    public void heal(int input) {
        items = player.getWeapons();
        int index = 0;

        for (Item item : items){
            if (item.getItemType() == ItemType.POTION) {
                if (input == index) {
                    if (item.calculateUsages()>= 0) {
                        player.healDamage(item.getAmount());

                    }else{
                        System.out.println("This item is broken, you heal no damage, visit the Shopkeeper to repair this potion or buy a new one");
                    }
                }
                else index++;
            }
        }
    }

    public void showCasinoOptions() {
        casino.showGames();

    }

    public void generateCasino() {
        casino = new Casino();
    }

    public int getMoney() {
        return player.getMoney();
    }

    public void coinflip(int input) {
        if (casino.coinflip()){
            System.out.println("You got heads, you won!");
            player.addMoney(input);
            System.out.println("Your current money is: " + getMoney());
        }else{
            System.out.println("You got tails, you lost!");
            player.subtractMoney(input);
            System.out.println("Your current money is: " + getMoney());
        }
    }

    public void rollDice() {
        player.subtractMoney(5);
        int diceroll = casino.rollDice();
        switch (diceroll){
            case 1 -> {
                System.out.println("You rolled a 1, you gain 1 coin");
                player.addMoney(1);
            }
            case 2 ->{
                System.out.println("You rolled a 2, you gain 3 coins");
                player.addMoney(3);
            }
            case 3 ->{
                System.out.println("You rolled a 3, you gain 5 coins");
                player.addMoney(5);
            }
            case 4 ->{
                System.out.println("You rolled a 4, you gain 7 coins");
                player.addMoney(7);
            }
            case 5 ->{
                System.out.println("You rolled a 5, you gain 12 coins");
                player.addMoney(12);
            }
            case 6 ->{
                System.out.println("You rolled a 6, you gain 20 coins");
                player.addMoney(20);
            }
        }
    }
    public void hitPinata(){
        if (firstPinataHit){
            player.subtractMoney(10);
            firstPinataHit = false;
        }
        player.addMoney(casino.hitPinata());
    }
    public boolean returnFirstPinata(){
        return firstPinataHit;
    }


}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ch.tbz.siad;

/**
 *
 * @author sierra
 */
public interface Creature {
    public void takeDamage(int amount);
    public int dealDamage();
    public void healDamage(int amount);
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ch.tbz.siad;

import java.util.InputMismatchException;

/**
 *
 * @author sierra
 */
public class InputException extends Exception{
    

    InputException(String only_number_inputs_are_allowed, InputMismatchException e) {
        super(only_number_inputs_are_allowed, e);    
    }

    
}

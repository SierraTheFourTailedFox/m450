/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ch.tbz.siad;

import java.util.ArrayList;

/**
 *
 * @author sierra
 */
public class ShopKeeper {
    ArrayList<Item> items = new ArrayList<>();
    void generateInventory(){
        items.add(new Item(5,1, false, 5, ItemType.ARMOR, "Tunic"));
        items.add(new Item(10,1, false, 10, ItemType.ARMOR, "Leather_Armor"));
        items.add(new Item(15,1, false, 15, ItemType.ARMOR, "Chainmail"));
        items.add(new Item(20,1, false, 25, ItemType.ARMOR, "Iron_Armor"));
        items.add(new Item(50,1, false, 40, ItemType.ARMOR, "Absorbuim_Armor"));
        items.add(new Item(10,1, false, 20, ItemType.WEAPON, "Dagger"));
        items.add(new Item(20,1, false, 40, ItemType.WEAPON, "Sword"));
        items.add(new Item(30,1, false, 60, ItemType.WEAPON, "Katana"));
        items.add(new Item(40,1, false, 80, ItemType.WEAPON, "Zweihander"));
        items.add(new Item(50,1, false, 100, ItemType.WEAPON, "Claymore"));
        items.add(new Item(1,1, true, 10, ItemType.POTION, "Once_Health_Restore_Potion"));
        items.add(new Item(1,1, true, 100, ItemType.POTION, "Once_Full_Health_Restore_Potion"));
        items.add(new Item(2,1, true, 10, ItemType.POTION, "Twice_Health_Restore_Potion"));
        items.add(new Item(2,1, true, 100, ItemType.POTION, "Twice_Full_Health_Restore_Potion"));
        items.add(new Item(3,1, true, 10, ItemType.POTION, "Trice_Health_Restore_Potion"));
        items.add(new Item(3,1, true, 100, ItemType.POTION, "Trice_Full_Health_Restore_Potion"));
    }

    public ArrayList<Item> getItems() {
        return items;
    }
    int getItemPrice(Item item){
        int price = (item.getAmount() + item.getDurability())/5;

        return price;
    }

}

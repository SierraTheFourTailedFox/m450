/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ch.tbz.siad;

/**
 *
 * @author sierra
 */
public class Item {
    private int durability;
    private int useageCost;
    private boolean canHeal;
    private int amount; // Depending on the type of the item (e.g. POTION for buff/heal, ARMOR for damage mitigation, WEAPON for damage), the amount will serve a different purpose
    private ItemType itemType;



    private String itemName;

    public Item(int durability, int decrease, boolean canHeal, int Amount, ItemType itemtype, String itemName){
        this.durability = durability;
        this.useageCost = decrease;
        this.canHeal = canHeal;
        this.amount = Amount;
        this.itemType = itemtype;
        this.itemName = itemName;
    }

    public void setUseageCost(int useageCost) {
        this.useageCost = useageCost;
    }

    public void setCanHeal(boolean canHeal) {
        this.canHeal = canHeal;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public int getUseageCost() {
        return useageCost;
    }

    public boolean isCanHeal() {
        return canHeal;
    }

    public int getAmount() {
        return amount;
    }

    public ItemType getItemType() {
        return itemType;
    }
    


    public int getDurability(){
        return durability;
    }
    public void setDurability(int amount){
        durability = amount;
    }
    public void enhanceDurability(int amount){
        durability += amount;
    }
    public void decreaseDurability(){
        durability -= useageCost;
    }

    public String getItemName() {return itemName;}

    public void setItemName(String itemName) {this.itemName = itemName;}

    public int calculateUsages(){
        int usages = durability/useageCost;
        return usages;
    }
}

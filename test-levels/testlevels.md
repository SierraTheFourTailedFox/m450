# Aufgabe 1
Was wird in unserer Firma getestet?

## Test Levels
Wir hatten so weit nur mit Acceptance-Testing zu tun, aber in der Art, dass einer von uns den Sser "gespielt" hat.
## Wann
Bei uns werden Tests zwischendurch und am Ende des Projekts ausgeführt, um zu wissen, wo wir das Projekt verbessern können, aber auf eine Art kann man sagen, dass wenn wir ein Stück Code geschrieben haben, testen wir direkt, ob dieses läuft.
## Testing Teams
Nein, wir haben kein Testing- oder Quality-Assurance Team.
## Live Cycle
Wir wissen nicht, wie der Live Cycle in unserer Firma aussieht.

# Aufgabe 2

## Test Approach
Definition, wie die Tester die Tests ausführen werden.
## Test Levels
Dies definiert, auf welcher Ebene (Unit, Integration, System oder Acceptance) der Test ausgeführt wird.
## Testing Types, Techniques and Tactics
Dies beschreibt, welche Tools man brauchen kann, oder ob man automatisiertes Testing implementieren kann. Es kann
auch beschreiben, welche Taktik man anwendet, also zum Beispiel wie viele Personen den Test ausführen.
## Zusammenhang
Mit diesen drei Punkten kann man entscheiden, auf welche Art man am effektivsten sein Programm testen kann. Die Arten müssen gut miteinander zusammenarbeiten können.
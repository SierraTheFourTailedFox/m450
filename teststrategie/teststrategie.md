# Teststrategie

## Übung 1

### Abstrakte Testfälle
1. Der Benutzer gibt einen Produktwert unter 15'000 ein, erwartet wird das kein Rabatt gegeben wird.

2. Der Benutzer gibt einen Produktwert zwischen 15'000 und 20'000 ein, erwartet werden 5% Rabatt.

3. Der Benutzer gibt einen Produktwert zwischen 20'000 und 25'000 ein, erwartet werden 7% Rabatt.

4. Der Benutzer gibt einen Produktwert höher als 25'000 ein, erwartet werden 8,5% Rabatt.

### Konkrete Testfälle
1. Der Benutzer gibt einen Produktwert von 15'000 ein, erwartet wird kein Rabatt und somit eine Ausgabe von 15000 als Preis.

2. Der Benutzer gibt einen Produktwert von 20'000 ein, erwartet wird ein Rabatt von 5% und somit eine Ausgabe von 19000 als Preis.

3. Der Benutzer gibt einen Produktwert von 25'000 ein, erwartet wird ein Rabatt von 7% und somit eine Ausgabe von 23'250 als Preis.

4. Der Benutzer gibt einen Produktwert von 30'000 ein, erwartet wird ein Rabatt von 8.5% und somit eine Ausgabe von 27'600 als Preis

## Übung 2

Test No | Aufgabe | Anmerkung
---|---|---
1 | Suche |
2 | Registrieren & Login| 
3 | Buchungen / Verwalten | 
4 | Lieblingsauto auswählen | 
5 | Bewertung hinterlassen | 
6 | Autotyp suchen | 
7 | Hamburger-Menü | 
8 | Cookies | 
9 | Sprache auswählen | 
10| Land auswählen | 

[economybookings](https://www.economybookings.com/)

## Übung 3

Bereich | Test | Anmerkung
---|---|---
1 | Konten anzeigen | 
1 | Konto erstellen  |
1 | Konto anzeigen mit neuem Konto|
1 | Wechselkurs anzeigen |
1 | Einzelnes Konto bearbeiten und verwalten
1 | Ungültige Eingaben |
2 | ExchangeRateOkhttp, Zeile 37 | Hier wird ein Return-Wert getestet.
2 | Counter, Zeile 45-47 | Hier wird ein Error bei falschen Eingaben gesendet.
2 | Main, Zeile 15-19 | Hier wird geschaut, ob die Konten richtig erstellt werden.
3 | - | Es gibt mehrere do-while (true) Loops. Diese würde ich nicht benutzen, da man es als rekursive Funktion schreiben könnte, und allgemein scheint eine while-true Schleife nicht die beste Option zu sein.

Bereiche:
- 1 Black box
- 2 White box
- 3 Anmerkung zum Code
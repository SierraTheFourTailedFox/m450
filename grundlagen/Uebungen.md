# Übungen

## Aufgabe 1

### Unit Test
Bei einem Unit-Test wird jedes einzelne Bestandteil für sich selbst getestet.

### "Box" - Tests
Bei Blackbox-Tests wird die Funktionalität von der Applikation getestet, ohne benötigtes Wissen über die Struktur oder den Code. Bei Whitebox-Tests wird die Applikation auf genau diese Dinge getestet, also zum Beispiel die Struktur und Implementation des Codes.

### Integration Tests
Bei Integration-Tests werden zwei oder mehr Bestandteile getestet, ob diese miteinander funktionieren.

## Aufgabe 2

### SW-Fehler
Das Programm führt die Anforderung nicht aus, zum Beispiel erwartet man eine Datei, bekommt aber nichts. Dies wäre ein Beispiel für einen Fehler.

### SW-Mangel
Das Programm führt die Anforderung zwar aus, aber sagen wir man erwartet eine Datei in welchem "Hello World" steht, würde man aber eine Datei kriegen in der nur "Hello" steht, wäre dies ein Mangel.

## Aufgabe 3
- ["test calculate prize"](testCalculatePrize.java)
## Aufgabe 3 - Bonus
Falls wir hier in Java programmieren, haben die if-else Statements keine geschweiften Klammern. Zudem fällt man immer in das erste if-statement, auch wenn das else-statement zutreffen sollte.
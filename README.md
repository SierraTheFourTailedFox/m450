# Testkonzept

## Introduction
The application we will be working on is an extension to a selfmade game called Defenderwaves 2.0.
We will create unit tests for this extension and possibly unit tests for the already exsisting Defenderwaves.

## Features to be tested
New features in Defenderwaves: Addition of a Casino as a way to make money, more types of enemies, and possibly a story and fraction of missions or quests, as well as SAVE feature.

## Features to not be tested
- Performance
- Non functional tests

## Approach
Test Driven Development

## Enviromental needs
Unit tests and manual tests

## Schedule
26.10.2023:
Writing first tests and start coding

02.11.2023:
Continuing coding

09.11.2023:
Finishing touches